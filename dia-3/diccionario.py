# EJERCICIO: Crear Modulo de diccionario que pueda dar de 
# Alta una clave y un valor
# eliminar-Baja,
# Actualizar y
# Recorrer


diccionario = dict()

diccionario = {
                "lunes" : "1",
                "martes" : "2",
                "miercoles" : "3",
                "jueves" : "4",
                "viernes" : "5",
                #"sabado" : "6",
                "domingo" : "7"
            }


diccionario["sabado"] = "6"
#diccionario.update({"sabado" : "6"})

print("-" * 70)
for par in diccionario.items():
    print(par[1],par[0],sep=" -> ")


del diccionario ["viernes"]
#diccionario.pop("viernes")


#print( diccionario.values() )
#print( diccionario.keys() )

#print ( diccionario.items() )

print("-" * 70)
for par in diccionario.items():
    print(par[1],par[0],sep=" -> ")
