import calculadora as calc
import diccionario as dic

def crear_alumno(d_alumnos, al): 
    if al is not None:
        dic.crear_elemento(d_alumnos, al)

def modificar_alumno(d_alumnos, al):
    if al[0] in d_alumnos:
        dic.modificar_elemento(d_alumnos, al)

def eliminar_alumno(d_alumnos, dni):
    if dni in d_alumnos:
        dic.eliminar_elemento(d_alumnos, dni)

def mostrar_alumnos(d_alumnos):
    if len(d_alumnos) > 0:
        dic.mostrar_elementos(d_alumnos)

def calcular_media_alumnos(d_alumnos):
    sumatorio_notas = 0
    media = None

    
    for alumno in d_alumnos:
       #("1111H", ("Juan", 9.5))
       sumatorio_notas = calc.sumar(sumatorio_notas, alumno[1][1])
    else:
       #media = sumatorio_notas / len(l_alumnos)
       media = calc.dividir(sumatorio_notas, len(d_alumnos))
    return media

def matricular_alumnos(d_aula,l_alumnos,l_dnis):
    for alumno in alumnos:
        dni = dni.pop(0)
        crear_alumno(d_aula,(dni,alumno))