
def crear_paquete(identificador, contenido, destino):
    return(identificador, contenido, destino)

def enviar_paquete(t_paquete):
    print(f"Enviando {t_paquete[1]} hacia {t_paquete[2]}..."")


def enviar_paquete_urgentes(s_paquetes):
    for paquete in s_paquetes:
        enviar_paquete(paquete)

def enviar_paquete_normales(s_paquetes):
    for paquete in s_paquetes:
        enviar_paquete(paquete)

def main():
    paquete_1 = crear_paquete(1,"consola xbox", "alicante")
    paquete_2 = crear_paquete(2,"jabon de lavadora", "barcelona")
    paquete_3 = crear_paquete(3,"paraguas plegable", "jaen")

    lista_paquetes = [paquete_1, paquete_2, paquete_3]

    paquetes_urgenes = set([paquete_1,paquete_3])
    paquetes_normales = set([paquete_2])


    print(len(paquetes_urgentes))
    enviar_paquetes_urgentes(paquetes.intersection(paquetes_urgentes))


if __name__ == "__main__": #hace referencia a que se trata del modulo principal
    main()