#import gestion_alumnos as gest_al
from gestion_alumnos import matricular_alumnos, calcular_media_alumnos

#fuente 1
alumnos = [ ("Juan", 9.5),
            ("Maria", 8.7),
            ("Carlos", 8.2)
          ]
          
#fuente 2
dnis = ["111H" ,"222H" ,"333H"]

aula = dict()

def informe_calificaciones(d_aula):
    print("lista alumnos:\n")
    mostrar_alumnos(d_aula)
    print("Fin lista alumnos.\n")

#gest_al.matricular_alumnos(aula,alumnos,dnis)
matricular_alumnos(aula,alumnos,dnis)

if len(aula) > 0:
    #resultado_media = gest_al.calcular_media_alumnos(aula)
    resultado_media = calcular_media_alumnos(aula)
    print("MEDIA AULA",round(resultado_media,2), sep=":") #el round es para redondear la media


