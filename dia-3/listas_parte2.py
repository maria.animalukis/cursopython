
nombre_completo = "Felipe Sanchez"
copia_nombre_completo = nombre_completo[:]

print("Nombre completo:", nombre_completo)
print("Nombre copmpleto (copia):", copia_nombre_completo)

print("ID_NC:",id(nombre_completo))
print("ID_CNC:",id(copia_nombre_completo))

#optimizacion
a = 1
b = 1
c = 2
print(id(a))
print(id(b))

#nombre_completo[0] = "H"  #inmutable
nombre_completo = 1

clientes = list()
#tupla // dupla (2 elementos)
cliente = ("Juan Lopez", "89889", False) #estructura de datos inmutable
print(type(cliente))
clientes.append(cliente)

clientes.append(("Maria", "7475", True))

print("Num clientes",len(clientes))

"""
maria_graduada = clientes[-1][-1]
if maria_graduada is not None:
    print("Maria ha demostrado que esta graduada")
else:
    print("Maria no ha podido constatar la graduacion")

Juan_graduado = clientes[0][2]
if Juan_graduado is not None:
    print("Juan ha demostrado que esta graduado")
else:
    print("Juan no ha podido constatar la graduacion")
"""

def cliente_graduado(cliente):
    """
    if cliente[-1] == True:
        return True
    else:
        return False
    """
    return cliente[-1] == True

"""
juan = clientes[0]
if cliente_graduado(juan):
    print("Juan graduado")

maria = clientes[-1]
if cliente_graduado(maria):
    print("Maria graduada")
    """
#nuevo cliente FUNCION
#1 crear tupla cliente
#2 añadirlo a la lista

def crear_y_anyadir_cliente(nombre, telefono, graduado, l_clientes):
    #crear la tupla
    cliente = (nombre, telefono, graduado)
    l_clientes.append(cliente)
    print("Cliente creado")

def listar_clientes(l_clientes):
    print("-" * 70)
    for registro in l_clientes:
        print (cliente_graduado(registro))
        print(registro[0])


def eliminar_cliente(l_clientes, nombre_cliente):
    for cliente in l_clientes:
        if cliente[0] == nombre_cliente:
            l_clientes.remove(cliente)
            break

def eliminar_clientes(l_clientes, l_clientes_a_eliminar):
    for cliente in l_clientes_a_eliminar:
        eliminar_cliente(l_clientes, cliente)


#invocar una funcion
crear_y_anyadir_cliente("Pedro", "123456", True, clientes)
listar_clientes(clientes)
eliminar_cliente(clientes, "Juan Lopez")
listar_clientes(clientes)

eliminar_clientes(clientes, ["Juan Lopez", "Pedro"])
listar_clientes(clientes)
