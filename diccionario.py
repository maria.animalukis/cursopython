def crear_elemento(diccio, elem): #elemento (tupla, clave...)
    diccio[elem[0]] = elem[1]

def modificar_elemento(diccio,nuevo_elemento):
    diccio[nuevo_elemento[0]] = nuevo_elemento[1]

def eliminar_elemento(diccio, clave_a_eliminar):
    del diccio[clave_a_eliminar]

def mostrar_elementos(diccio):
    for elemento in diccio.items():
        print(elemento[0], elemento[1], sep=" -> ")