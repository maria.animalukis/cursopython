#asignar la variable nombre

#persona
nombre = "Maria" #str
apellido = "Roque" #str
edad = 7 #int
nota = 4.45 #float

print(f"Hola, me llamo {nombre} y tengo {edad} años")

#alternativa
print("Hola, me llamo", nombre, "y tengo", edad, "años")

aprobado = nota > 5.5 #si nota es 4.45, devuelve False
print("Tipo de dato de la var. aprobado:",type(aprobado))

#estructura condicional
if nota > 5.5: #todo aquello que devuelve verdadero o falso, condicion
    print("has aprobado")
else:
    print("No has aprobado")

"""
voy a evaluar con
la variable edad
"""
if edad >= 6 and edad < 15:
    print("Curso primaria")
elif edad == 15:
    print("Curso ESO")
elif edad == 19:
    print("Curso universitario")
else:
    print("Otros cursos")


#resultadoOpos
experiencia_laboral = 8
puntos_extra = 10 if experiencia_laboral >= 10 else 0 #expression if, one-liner
if puntos_extra == 10:
    print("Tienes puntuacion adicional")
else:
    print("No tienes puntuacion adicional")


if nota > 5.5 and nota < 8.75:
    print("Aprobado")

#string
#snake notation
nombre_completo = f"{nombre} {apellido}"

#"Maria Roque"


primer_caracter = nombre_completo[0]
print("Primer caracter",primer_caracter, sep=":")

print("Longitud del str:", len(nombre_completo))

ultimo_caracter = nombre_completo[len(nombre_completo) - 1]
#print("Ult. caracter",ultimo_caracter, sep=":")

#pythoniana
ultimo_caracter = nombre_completo[-1]
print("Ult. caracter",ultimo_caracter, sep=":")

#"Maria Roque"
pos_espacio = nombre_completo.index(" ")
print("Pos espacio:", pos_espacio)
nombre = nombre_completo[0:pos_espacio]
print("Nombre:", nombre)

apellido = nombre_completo[pos_espacio + 1:]
print("Apellido", apellido)

#[4:-2] Maria Roque -> ria roq
print(nombre_completo[2:-2])
""""
#nombre_completo recorrido (estructuras iterativas)
for caracter in nombre_completo:
    print("Car:", caracter.upper())
"""

i = 0
while i < len(nombre_completo):
    print("Car:", nombre_completo[i])
    i = i + 1

myint = 7
midecimal = 7
print("mi decimal es",midecimal)

a , b = 3, 4 #mes, día
print("nací en el mes", a, "el día", b)
print(f"nací en el mes {a} el dia {b}")


