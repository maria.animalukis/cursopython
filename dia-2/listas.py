#listas

#"Juan Perez" Coleccion de caracteres

#coleccion de nombres Juan, Maria, Marcos, Silvia
nombres = ["Juan", "Maria", "Marcos", "Silvia"] #list

print("Tipo de dato de la var. nombres:", type(nombres))

#re4correr coleccion
for nombre in nombres:
    print("Nombre", nombre, sep=":")

#el primer caracter de cada elemento de la lista nombres
for nombre in nombres:
    print("Primer caracter", nombre[0],"Ultimo caracter",nombre[-1])

#ver todos los nombres en mayusculas
for nombre in nombres:
    print(nombre[:1].upper())

bolsa_playa = [] #lista vacia
#toalla, cerezas, parasol, crema, chancletas, flotador pato
bolsa_playa.append("toalla")
bolsa_playa.append("cervezas")
bolsa_playa.append("parasol")
bolsa_playa.append("crema")
bolsa_playa.append("chancletas")
bolsa_playa.append("flotador pato")

print(len(bolsa_playa))
print("Num toallas:", bolsa_playa.count("toalla"))

#añadir toalla
bolsa_playa.append("toalla")
print("Num toallas:", bolsa_playa.count("toalla"))

bolsa_playa.insert(0, "cervezas")
print("Num cervezas:", bolsa_playa.count("cervezas"))

cervezas_primera_posicion = bolsa_playa[0] == "cervezas"
hay_cervezas = bolsa_playa.index("cervezas") >= 0

if hay_cervezas:
    print("vamos a la playa")
else:
    print("coge cervezas")

print("-" * 60) #separador

for item_playa in bolsa_playa:
    print(item_playa)

#quitar cervezas
bolsa_playa.remove("cervezas")
print("Num cervezas:", bolsa_playa.count("cervezas"))
if bolsa_playa.count("cervezas") > 0:
    bolsa_playa.remove("cervezas")

print("Num cervezas:", bolsa_playa.count("cervezas"))

#estamos en playa
"""
sacamos las toallas
sacamos la crema
"""
pos_toalla = bolsa_playa.index("toalla")
if pos_toalla >= 0:
    toalla = bolsa_playa.pop(pos_toalla)

#aseguro que hay crema
crema = bolsa_playa.pop(bolsa_playa.index("crema"))

for item in bolsa_playa:
    print("Item:",item)

item_a_eliminar = input("Que quieres eliminar de la bolsa?")
if len(item_a_eliminar) > 0:
    bolsa_playa.remove(item_a_eliminar)

#comprobacion
for item in bolsa_playa:
    print(item)

