number = str(input("Introduce numero 0 a 99: "))

unidades = {
              0 :	'cero',
              1 :	'uno',
              2 :	'dos',	
              3 :	'tres',	
              4 :	'cuatro',	
              5 :	'cinco',	
              6 :	'seis',	
              7 :	'siete',	
              8 :	'ocho',	
              9 :	'nueve'
          }

diezes  =  {
              10 : 'diez',
              11	: 'once',	
              12	: 'doce',	
              13	: 'trece',	
              14	: 'catorce',	
              15	: 'quince',
              16	: 'dieciséis',
              17	: 'diecisiete',	
              18	: 'dieciocho',	
              19	: 'diecinueve'
}

decenas = {
            1 : 'diez',	
            2 : 'veinte',
            3 : 'treinta',
            4 : 'cuarenta',	
            5 : 'cincuenta',	
            6 : 'sesenta',
            7 : 'setenta',	
            8 : 'ochenta',	
            9 : 'noventa'	
}

if  (len (number)) == 1:
  print (unidades[int(number)])
elif ( int(number) >= 10 and int(number) <= 19 ):
  print (diezes[ int( number) ])
elif ( int(number) >= 21 and int(number) <= 29 ):
  print ( "veinti" +  (unidades[ int(number[1]) ])  )
elif ( int(number[1]) == 0 ):
  print (decenas[int(number[0])])
else:
  print ( (decenas[ int(number[0]) ] )   + " y " +  (unidades[ int(number[1]) ])  )
